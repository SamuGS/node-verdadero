var http = require('http');
var puerto = 3000;

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end('Hola Mundo!');
  console.log("Servidor corriendo en el puerto "+puerto);
}).listen(puerto); 