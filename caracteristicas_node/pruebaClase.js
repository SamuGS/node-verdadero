var http = require('http');
var puerto = 3000;

http.createServer(function (req, res) {
    var square = require('./modulos.js'); // Here we require() the name of the file without the (optional) .js file extension
    console.log('The area of a square with a width of 4 is ' + square.area(4));
}).listen(puerto);