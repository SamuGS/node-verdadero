// wiki.js - Wiki route module

const express = require('express');
const about = require('./about.js');
const app = express();
const port = 3000;

app.use('/about',about);

app.listen(port, () => {
    console.log('Ejemplo de uso de express');
  });