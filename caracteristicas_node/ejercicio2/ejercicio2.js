const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    var modulos = require('./modulos'); 
    res.end('La fecha y hora actual es: ' + modulos.actual.toString());
});

app.listen(port, () => {
  console.log('Ejemplo de uso de express');
});