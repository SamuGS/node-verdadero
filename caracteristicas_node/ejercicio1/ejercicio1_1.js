var calculadora = require('./modulos');
var num1 = 20;
var num2 = 12;
var solucion;
var opcion = "suma";
console.log("La " + opcion + " de " + num1 + " y " + num2 + " es: ");
switch (opcion) {
    case "suma":
        var solucion = calculadora.suma(num1, num2);
        break;
    case "resta":
        var solucion = calculadora.resta(num1, num2);
        break;
    case "multiplicacion":
        var solucion = calculadora.multiplicacion(num1, num2);
        break;
    case "division":
        var solucion = calculadora.division(num1, num2);
        break;
}
console.log(solucion);