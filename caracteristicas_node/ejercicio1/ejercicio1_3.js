var http = require('http');
var puerto = 3000;

http.createServer(function (req, res, res2) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    var modulos = require('./modulos'); 
    var fecha = modulos.actual.getDate() + "-" + (modulos.actual.getMonth() + 1) + "-" + modulos.actual.getFullYear();
    var hora = modulos.actual.getHours() + ":" + modulos.actual.getMinutes() + ":" + modulos.actual.getSeconds();
    var tiempoActual = fecha + " " + hora;
    res.end('La fecha y hora actual es: ' + tiempoActual);

}).listen(puerto);